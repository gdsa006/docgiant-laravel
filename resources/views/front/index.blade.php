@extends('front.layout')

@section('main')


<!--Navbar-->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar  g-py-10" id="mainNav">

  <!-- Navbar brand -->
  <a class="navbar-brand white-logo" href="#"><img src="/images/logo1.png" width="173px" height="35px"></a>
  <a class="navbar-brand blue-logo" href="#"><img src="/images/logo2.png" width="173px" height="35px"></a>
  <!-- Collapse button -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav" aria-controls="basicExampleNav"
  aria-expanded="false" aria-label="Toggle navigation">
  <span class="navbar-toggler-icon"></span>
</button>

<!-- Collapsible content -->
<div class="collapse navbar-collapse" id="basicExampleNav">

  <!-- Links -->
  <ul class="navbar-nav ml-auto">
     <li class="nav-item">
      <a class="nav-link js-scroll-trigger" href="#home">Home</a>
  </li>
  <li class="nav-item">
      <a class="nav-link js-scroll-trigger" href="#about">About</a>
  </li>
  <li class="nav-item">
    <a class="nav-link js-scroll-trigger" href="#why">Why DocGiant</a>
</li>
<li class="nav-item">
    <a class="nav-link js-scroll-trigger" href="#blog">Blog</a>
</li>
<li class="nav-item d-none">
    <a class="nav-link js-scroll-trigger" href="#demo">Request Demo</a>
</li>

</ul>
<!-- Links -->

<form class="form-inline d-none">
  <div class="md-form mt-0">
      <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
  </div>
</form>
</div>
<!-- Collapsible content -->

</nav>
<!--/.Navbar-->



<header class="masthead" id="home" style="z-index:1;background-image: url(https://imgs.6sqft.com/wp-content/uploads/2018/01/31122327/99-Hudson-5-e1517500179504.jpg);
background-repeat: no-repeat;
background-size: cover;
background-position: center center; 
cursor: default;">  










<div class="view" style="background-image: url('https://imgs.6sqft.com/wp-content/uploads/2018/01/31122327/99-Hudson-5-e1517500179504.jpg'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
  <div class="mask rgba-gradient d-flex justify-content-center align-items-center">
      <div class="container">
        <div class="row">




          <div class="col-md-6 white-text text-center text-md-left mt-xl-5 mb-5 wow fadeInLeft d-none d-sm-block" data-wow-delay="0.3s" style="visibility: visible; animation-name: fadeInLeft; animation-delay: 0.3s;">
            <h1 class="h1-responsive font-weight-bold mt-sm-5">The game changing capability you need</h1>
            <hr class="hr-light">
            <h6 class="mb-4">We are your partners for healthcare and medical record processing. We believe in knowledge.

                Knowledge is more than just data. We believe in extreme speed. Because, speed is valuable. 

            </h6>
            <!-- <a class="btn btn-white waves-effect waves-light">Download</a> -->
            <a class="btn btn-outline-white waves-effect waves-light">Learn more</a>
        </div>

















        <div class="col-md-6 col-xl-5 mt-xl-5 wow fadeInRight">

          <!-- form card register -->
          <div class="card card-outline-secondary float-right shadow" style="width:400px">
            <div class="card-header">
              <h3 class="mb-0">Write to us</h3>
          </div>
          <div class="card-body">
              <form action="{{ url('/') }}" method="post" class="form" role="form" autocomplete="off">
                {{ csrf_field() }}
                <div class="form-group">
                  <div class="md-form">
                    <input type="text" id="fullname" name="fullname" class="form-control">
                    <label for="fullname">Full Name</label>
                </div>
            </div>
            <div class="form-group">
              <div class="md-form">
                <input type="email" id="email" name="email" class="form-control">
                <label for="email">Email</label>
            </div>
        </div>

        <div class="form-group">
          <div class="md-form">
            <textarea type="text" id="materialFormContactMessageEx" name="msg" class="form-control md-textarea" rows="3"></textarea>
            <label for="fullname">Message</label>
        </div>
    </div>



    @if(Session::has('success'))
            <div class="form-group">
                   <div class="text-center py-4 mt-3">
                       <button class="btn btn btn-outline-success" disabled><i class="fa fa-check-circle"></i>Message Sent! Thanks.</button>
                   </div>
               </div>
        
@else


    <div class="form-group">
       <div class="text-center py-4 mt-3">
           <button class="btn btn-cyan" type="submit">Request Demo</button>
       </div>
   </div>
   @endif



</form>




</div>
</div>
</div>
</div>
<!-- /form card register -->








</div>
</div>
</div>
</header>

<!-- Services -->
<section id="about" class="about edge--top--reverse" >
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h1 class="section-heading text-uppercase font-weight-bold">Medical Records. Organized. Delivered.</h1>
          <h3 class="section-subheading text-muted">People, Process and Platform</h3>
      </div>
  </div>
  <div class="row no-gutters mt-5">
    <div class="col-lg-4 g-px-40 g-mb-50 g-mb-0--lg grid">
      <!-- Icon Blocks -->
      <div class="text-center">
        <span class="d-inline-block u-icon-v3 u-icon-size--xl g-bg-primary g-color-white rounded-circle g-mb-30">
          <img src=/images/book.png class="img-responsive" width="70px">
      </span>
      <h3 class="h5 g-color-gray-dark-v2 g-font-weight-600 text-uppercase mb-3">Our Story</h3>
      <p class="mb-0 px-4">With decades of healthcare experience, we are driven to transform healthcare processes with knowledge workers, process optimization and technology automation.</p>
  </div>
  <!-- End Icon Blocks -->
</div>

<div class="col-lg-4 g-brd-left--lg g-brd-gray g-px-40 g-mb-50 g-mb-0--lg grid">
  <!-- Icon Blocks -->
  <div class="text-center">
    <span class="d-inline-block u-icon-v3 u-icon-size--xl g-bg-primary g-color-white rounded-circle g-mb-30">
      <img src=/images/mind.png class="img-responsive" width="70px">
  </span>
  <h3 class="h5 g-color-gray-dark-v2 g-font-weight-600 text-uppercase mb-3">Our Vision</h3>
  <p class="mb-0 px-4">We believe in delivering for your requirements today. And building for a future imagined together.

  </p>
</div>
<!-- End Icon Blocks -->
</div>

<div class="col-lg-4 g-brd-left--lg g-brd-gray-light-v4 g-px-40 grid">
  <!-- Icon Blocks -->
  <div class="text-center">
    <span class="d-inline-block u-icon-v3 u-icon-size--xl g-bg-primary g-color-white rounded-circle g-mb-30">
      <img src=/images/interactivity.png class="img-responsive" width="70px">
  </span>
  <h3 class="h5 g-color-gray-dark-v2 g-font-weight-600 text-uppercase mb-3">Technology</h3>
  <p class="mb-0 px-4">DocGiant platform enables automation, insights and delivery speed.</p>
</div>
<!-- End Icon Blocks -->
</div>
</div>

</div>
</section>

<!-- Portfolio Grid -->
<section class="why edge--top" id="why">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h1 class="section-heading text-uppercase font-weight-bold">Why DocGiant?</h1>
          <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
      </div>




      <div class="row mt-5">

        <!--Grid column-->
        <div class="col-lg-6 col-xl-5 pr-lg-5 pb-5 img">

            <!--Image-->
            <img src="/images/why1.jpg" alt="Sample project image" class="img-fluid rounded z-depth-1">

        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-6 col-xl-7 pl-lg-5 pb-4">

            <!--Grid row-->
            <div class="row mb-3">
               
                <div class="col-10">
                    <h1 class="font-weight-bold">Fast</h1>
                    <h5 class="grey-text">We believe in extreme speed. Because, speed is valuable. </h5>
                    <p>We deliver in 24 hours.</p>
                </div>
            </div>
            <!--Grid row-->

            <!--Grid row-->
            <div class="row mb-3 d-none">
                <div class="col-1 mr-1">
                    <i class="fa fa-code fa-2x red-text"></i>
                </div>
                <div class="col-10 ">
                    <h5 class="font-weight-bold">Technology</h5>
                    <p class="grey-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit maiores nam, aperiam
                    minima assumenda voluptate velit.</p>
                </div>
            </div>
            <!--Grid row-->

            <!--Grid row-->
            <div class="row d-none">
                <div class="col-1 mr-1">
                    <i class="fa fa-money fa-2x deep-purple-text"></i>
                </div>
                <div class="col-10">
                    <h5 class="font-weight-bold">Finance</h5>
                    <p class="grey-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit maiores nam, aperiam
                    minima assumenda voluptate velit.</p>
                </div>
            </div>
            <!--/Grid row-->

        </div>
        <!--Grid column-->

    </div>

    <div class="row pt-5">

        <!--Grid column-->
        <div class="col-lg-6 col-xl-7 mb-3 d-none d-sm-block">

            <!--Grid row-->
            <div class="row mb-3">
                <div class="col-1 mr-1">
                    <i class="fa fa-bar-chart fa-2x indigo-text"></i>
                </div>
                <div class="col-10">
                    <h1 class="font-weight-bold">Smart</h1>
                    <h5 class="grey-text">We believe in knowledge.

Knowledge is more than just data.</h5>
<p>We deliver organized knowledge.

</p>
                </div>
            </div>
            <!--Grid row-->

            <!--Grid row-->
            <div class="row mb-3 d-none">
                <div class="col-1 mr-1">
                    <i class="fa fa-music fa-2x pink-text"></i>
                </div>
                <div class="col-10">
                    <h5 class="font-weight-bold">Entertainment</h5>
                    <p class="grey-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit maiores nam, aperiam
                    minima assumenda voluptate velit.</p>
                </div>
            </div>
            <!--Grid row-->

            <!--Grid row-->
            <div class="row mb-3 d-none">
                <div class="col-1 mr-1">
                    <i class="fa fa-smile-o fa-2x blue-text"></i>
                </div>
                <div class="col-10">
                    <h5 class="font-weight-bold">Communication</h5>
                    <p class="grey-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit maiores nam, aperiam
                    minima assumenda voluptate velit.</p>
                </div>
            </div>
            <!--/Grid row-->

        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-6 col-xl-5 mb-3 pb-5 img">

            <!--Image-->
            <img src="/images/why2.jpg" alt="Sample project image" class="img-fluid rounded z-depth-1">

        </div>
                <div class="col-lg-6 col-xl-7 mb-3 d-block d-sm-none">

                    <!--Grid row-->
                    <div class="row mb-3">
                        <div class="col-1 mr-1">
                            <i class="fa fa-bar-chart fa-2x indigo-text"></i>
                        </div>
                        <div class="col-10">
                            <h1 class="font-weight-bold">Smart</h1>
                            <h5 class="grey-text">We believe in knowledge.

        Knowledge is more than just data.</h5>
        <p>We deliver organized knowledge.

        </p>
                        </div>
                    </div>
                    <!--Grid row-->

                    <!--Grid row-->
                    <div class="row mb-3 d-none">
                        <div class="col-1 mr-1">
                            <i class="fa fa-music fa-2x pink-text"></i>
                        </div>
                        <div class="col-10">
                            <h5 class="font-weight-bold">Entertainment</h5>
                            <p class="grey-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit maiores nam, aperiam
                            minima assumenda voluptate velit.</p>
                        </div>
                    </div>
                    <!--Grid row-->

                    <!--Grid row-->
                    <div class="row mb-3 d-none">
                        <div class="col-1 mr-1">
                            <i class="fa fa-smile-o fa-2x blue-text"></i>
                        </div>
                        <div class="col-10">
                            <h5 class="font-weight-bold">Communication</h5>
                            <p class="grey-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit maiores nam, aperiam
                            minima assumenda voluptate velit.</p>
                        </div>
                    </div>
                    <!--/Grid row-->

                </div>
        <!--Grid column-->

    </div>









</div>
</div>
</div>
</section>














  <!-- demo -->
 <!--  <section id="demo" class="demo overlay edge--top edge--bottom">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Demo</h2>
          <h3 class="section-subheading text-muted">To schedule a product demo with one of our product consultants, please fill in your contact details
          </h3>
      </div>
  </div>

</div>
</section> -->




<section id="demo" class="demo overlay edge--top--reverse">
  <div class="container">
     <div class="row">

    <div class="col-lg-12 text-center">
                     <h1 class="section-heading text-uppercase font-weight-bold">Testimonials</h1>
                     <h3 class="section-subheading text-muted">What our customer says.</h3>
                 </div>
          



                      <div id="myCarousel" class="carousel slide fade-carousel mt-5" data-ride="carousel">
                        <!-- Carousel indicators -->
                        <ol class="carousel-indicators">
                          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                          <li data-target="#myCarousel" data-slide-to="1"></li>
                          <li data-target="#myCarousel" data-slide-to="2"></li>
                        </ol>   
                        <!-- Wrapper for carousel items -->
                        <div class="carousel-inner">
                          <div class="item carousel-item active">
                            <div class="row">
                              <div class="col-sm-6">
                                <div class="media">
                                  <div class="media-left d-flex mr-3">
                                    <a href="#">
                                      <img src="/examples/images/clients/1.jpg" alt="">
                                    </a>
                                  </div>
                                  <div class="media-body">
                                    <div class="testimonial">
                                      <p>Lorem ipsum dolor sit amet, consec adipiscing elit. Nam eusem scelerisque tempor, varius quam luctus dui. Mauris magna metus nec.</p>
                                      <p class="overview"><b>Paula Wilson</b>, Media Analyst</p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="col-sm-6">
                                <div class="media">
                                  <div class="media-left d-flex mr-3">
                                    <a href="#">
                                      <img src="/examples/images/clients/2.jpg" alt="">
                                    </a>
                                  </div>
                                  <div class="media-body">
                                    <div class="testimonial">
                                      <p>Vestibulum quis quam ut magna consequat faucibus. Pellentesque eget mi suscipit tincidunt. Utmtc tempus dictum. Pellentesque virra.</p>
                                      <p class="overview"><b>Antonio Moreno</b>, Web Developer</p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>      
                          </div>
                          <div class="item carousel-item">
                            <div class="row">
                              <div class="col-sm-6">
                                <div class="media">
                                  <div class="media-left d-flex mr-3">
                                    <a href="#">
                                      <img src="/examples/images/clients/3.jpg" alt="">
                                    </a>
                                  </div>
                                  <div class="media-body">
                                    <div class="testimonial">
                                      <p>Lorem ipsum dolor sit amet, consec adipiscing elit. Nam eusem scelerisque tempor, varius quam luctus dui. Mauris magna metus nec.</p>
                                      <p class="overview"><b>Michael Holz</b>, Seo Analyst</p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="col-sm-6">
                                <div class="media">
                                  <div class="media-left d-flex mr-3">
                                    <a href="#">
                                      <img src="/examples/images/clients/4.jpg" alt="">
                                    </a>
                                  </div>
                                  <div class="media-body">
                                    <div class="testimonial">
                                      <p>Vestibulum quis quam ut magna consequat faucibus. Pellentesque eget mi suscipit tincidunt. Utmtc tempus dictum. Pellentesque virra.</p>
                                      <p class="overview"><b>Mary Saveley</b>, Web Designer</p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>      
                          </div>
                          <div class="item carousel-item">
                            <div class="row">
                              <div class="col-sm-6">
                                <div class="media">
                                  <div class="media-left d-flex mr-3">
                                    <a href="#">
                                      <img src="/examples/images/clients/5.jpg" alt="">
                                    </a>
                                  </div>
                                  <div class="media-body">
                                    <div class="testimonial">
                                      <p>Lorem ipsum dolor sit amet, consec adipiscing elit. Nam eusem scelerisque tempor, varius quam luctus dui. Mauris magna metus nec.</p>
                                      <p class="overview"><b>Martin Sommer</b>, UX Analyst</p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="col-sm-6">
                                <div class="media">
                                  <div class="media-left d-flex mr-3">
                                    <a href="#">
                                      <img src="/examples/images/clients/6.jpg" alt="">
                                    </a>
                                  </div>
                                  <div class="media-body">
                                    <div class="testimonial">
                                      <p>Vestibulum quis quam ut magna consequat faucibus. Pellentesque eget mi suscipit tincidunt. Utmtc tempus dictum. Pellentesque virra.</p>
                                      <p class="overview"><b>John Williams</b>, Web Developer</p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>      
                          </div>
                        </div>
                      </div>
                   

  

</div>



  </section>

  <!-- About -->
  <section id="blog" class="blog edge--top">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h1 class="section-heading text-uppercase font-weight-bold">Blog</h1>
            <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
        </div>

        @isset($info)
        @component('front.components.alert')
        @slot('type')
        info
        @endslot
        {!! $info !!}
        @endcomponent
        @endisset
        @if ($errors->has('search'))
        @component('front.components.alert')
        @slot('type')
        error
        @endslot
        {{ $errors->first('search') }}
        @endcomponent
        @endif  

        <div class="row no-gutters mt-5">
          <div class="card-deck">



          @foreach($chunk as $post)

          @include('front.brick-standard', compact('$post'))

          @endforeach


  </div>
      </div>

   <!--  -->
  </div>

  </div>
  </section>















@endsection
