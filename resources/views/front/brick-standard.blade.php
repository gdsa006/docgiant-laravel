

<!--Card Light-->
<div class="card mb-4">
    <!--Card image-->
    <div class="view overlay">
        <img src="{{ $post->image }}" class="img-fluid" alt="fern">
        <a>
            <div class="mask rgba-white-slight"></div>
        </a>
    </div>
    <!--/.Card image-->
    <!--Card content-->
    <div class="card-body">
        <!--Social shares button-->
        <a class="activator p-3 mr-2"><i class="fa fa-share-alt"></i></a>
        <!--Title-->
        <h4 class="card-title">{{ $post->title }}</h4>
        <hr>
        <!--Text-->
        <p class="card-text">            {{ $post->excerpt }}
</p>
        <a class="link-text" href="{{ url('posts/' . $post->slug) }}"><h5>Read more <i class="fa fa-chevron-right"></i></h5></a>
    </div>
    <!--/.Card content-->
</div>
<!--/.Card Light-->