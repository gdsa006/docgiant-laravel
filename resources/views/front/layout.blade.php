<!DOCTYPE html>
<!--[if IE 8 ]><html class="no-js oldie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="no-js oldie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" lang="{{ config('app.locale') }}"> <!--<![endif]-->
<head>

	<!--- basic page needs
   ================================================== -->
   <meta charset="utf-8">
   <title>{{ isset($post) && $post->seo_title ? $post->seo_title :  __(lcfirst('Title')) }}</title>
   <meta name="description" content="{{ isset($post) && $post->meta_description ? $post->meta_description : __('description') }}">
   <meta name="author" content="@lang(lcfirst ('Author'))">
   @if(isset($post) && $post->meta_keywords)
   <meta name="keywords" content="{{ $post->meta_keywords }}">
   @endif
   <meta name="csrf-token" content="{{ csrf_token() }}">

	<!-- mobile specific metas
   ================================================== -->
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

   <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
   <link rel="stylesheet" href="/bootstrap-material-design/dist/css/mdb.css">

   <link rel="stylesheet" href="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/assets/owl.carousel.min.css">
   <link rel="stylesheet" href="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/assets/owl.theme.default.min.css">
   @yield('css')
   <link href="https://fonts.googleapis.com/css?family=Maven+Pro" rel="stylesheet">
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">


   <style>
   .search-wrap .search-form::after {
     content: "@lang('Press Enter to begin your search.')";
   }

 </style>


	<!-- script
   ================================================== -->
   <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>

	<!-- favicons
   ================================================== -->
   <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
   <link rel="icon" href="favicon.ico" type="image/x-icon">

 </head>

 <body id="page-top">



   @yield('main')

   <!-- footer
     ================================================== -->
     <footer style="padding: 20px 0">

      <div class="footer-main">
        <div class="container">
         <div class="row">


          <div class="col-md-6 justify-content-center align-self-center">
           <span class="float-left">
             &copy; 2018 by DocGiant LLC
           </span>
         </div>

         <div class="col-md-6 justify-content-center align-self-center">
           <span class="float-right  justify-content-center align-self-center">
             <ul class="social-network social-circle">
               <li><a href="#" class="icoRss" title="Rss"><i class="fab fa-facebook-f"></i>

               </a></li>
               <li><a href="#" class="icoFacebook" title="Facebook"><i class="fab fa-twitter"></i></a></li>
               <li><a href="#" class="icoTwitter" title="Twitter"><i class="fab fa-google-plus-g"></i></a></li>

             </ul>      
           </span>

         </div>
       </div>
     </div>
   </div> <!-- end footer-bottom -->

 </footer>

 <div id="preloader">
   <div id="loader"></div>
 </div>
 <script src="{{ mix('/js/app.js') }}"></script>

   <!-- Java Script
     ================================================== -->
     <script src="https://code.jquery.com/jquery-3.2.0.min.js"></script>
     <script src="{{ asset('js/plugins.js') }}"></script>
     <script src="{{ asset('js/main.js') }}"></script>
     <script src="https://gdsa006.bitbucket.io/assets/jquery-easing/jquery.easing.min.js"></script>

     <script src="/bootstrap-material-design/dist/js/mdb.js"></script>


     @yield('scripts')
     <script type="text/javascript">
       $(window).scroll(function() {
         var nav = $('#mainNav');
         var top = 200;
         if ($(window).scrollTop() >= top) {

           nav.addClass('white-nav');

         } else {
           nav.removeClass('white-nav');
         }
       });
     </script>

     <script>
      $(function() {
       $('#logout').click(function(e) {
        e.preventDefault();
        $('#logout-form').submit()
      })
     })
   </script>
   <script type="text/javascript">
   // Activate scrollspy to add active class to navbar items on scroll
   $('body').scrollspy({
    target: '#mainNav',
    offset: 56
  });

    // Collapse Navbar
    var navbarCollapse = function() {
      if ($("#mainNav").offset().top > 100) {
        $("#mainNav").addClass("navbar-shrink");
      } else {
        $("#mainNav").removeClass("navbar-shrink");
      }
    };
    })(jQuery); // End of use strict
  </script>
</body>

</html>
