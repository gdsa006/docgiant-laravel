let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css/app.css') .copy('node_modules/mdbootstrap/js', 'public/bootstrap-material-design/dist/js')
    .copy('node_modules/mdbootstrap/css', 'public/bootstrap-material-design/dist/css').copy('node_modules/jquery.easing', 'public/jquery.easing/dist/js');
