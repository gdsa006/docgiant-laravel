<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Mail;
use Session;
class PagesController extends Controller
{
    public function postContact(Request $request){
    	$this->validate($request, [
    		'email' => 'required|email',
    		'message' => 'min:10'
    	]);
    	$data = array(
    		'fullname' => $request->fullname,
    		'email' => $request->email,
    		'msg' => $request->msg
    	);
    	Mail::send('front.emails.contact', $data, function($message) use($data){
    		$message->from($data['email']);
    		$message->to('hsingh@contactmydoc.com');
    		$message->subject('Contact Via Contact Form');
    	});
    	Session::flash('success', 'Your demo request has been sent!');
    	return back();
    	}
}
